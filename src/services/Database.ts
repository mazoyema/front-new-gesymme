class Database {

    private readonly connection;

    constructor() {
    }

    private async start(){
        try {
            console.log("Successfully connected to the database")
        } catch (error) {
            console.error('Unable to connect to the database:', error)
        }
    }

}

export default new Database()
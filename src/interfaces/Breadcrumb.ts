export type Breadcrumbs = Breadcrumb[];

export interface Breadcrumb {
    title: string,
    url?: string
}

export const HomeBreadcrumb: Breadcrumb = {
    title: 'Accueil',
    url: '/'
}

export const InformationsBreadcrumb: Breadcrumb = {
    title: 'Mes informations',
    url: '/information'
}

export const OfficesBreadcrumb: Breadcrumb = {
    title: 'Mes bureaux',
    url: '/offices'
}

export const ContactsBreadcrumb: Breadcrumb = {
    title: 'Mes coordonnées',
    url: '/contacts'
}

export const SocialsBreadcrumb: Breadcrumb = {
    title: 'Mes réseaux sociaux',
    url: '/socials'
}

export const ResponsibilitesBreadcrumb: Breadcrumb = {
    title: 'Mes responsabilités',
    url: '/responsibilites'
}

export const CurriculumBreadcrumb: Breadcrumb = {
    title: 'Mon CV',
    url: '/curriculum'
}

export const ActivitiesBreadcrumb: Breadcrumb = {
    title: 'Mes activités',
    url: '/activities'
}